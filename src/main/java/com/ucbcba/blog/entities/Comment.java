package com.ucbcba.blog.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by amolina on 26/09/17.
 */
@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    @Size(min = 1,max = 50,message = "El Comentario debe ser mayor a 1 y menor a 100 letras")
    private String text;

    @NotNull
    @Column(columnDefinition="int(5) default '0'")
    private Integer likes = 0 ;

    @NotNull
    @ManyToOne
    @JoinColumn(name="post_id")
    private Post post;

    public Comment(){

    }
    public Comment(Post post){
        this.post = post;

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }
}
